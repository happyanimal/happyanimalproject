package com.coderiders.happyanimal.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InspectionRsDto {
    @NotNull
    @Min(1L)
    private Long id;
    @NotNull
    private String date;
    @NotNull
    private List<AnimalRsDto> animalList;
}
