package com.coderiders.happyanimal.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskRqDto {
    @NotNull
    private String type;
    @NotNull
    private LocalDateTime expiresDateTime;
    @NotNull
    private String repeatType;
    @NotNull
    private boolean isCompleted;
    @NotNull
    private Long animalId;

    private String note;
}
