package com.coderiders.happyanimal.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WeatherDto {
    private String country;
    private String region;
    private double tempC;
    private String text;
    private double windKph;
    private double pressureMb;
    private double precipMm;
    private int humidity;
    private List<ForecastDayDto> forecastDayList;
}
