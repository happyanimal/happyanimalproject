package com.coderiders.happyanimal.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Kind")
public class AnimalKind {

    @Id
    @Column(name = "kind")
    private String kind;

    @Column(name = "class")
    private String animalClass;

    @Column(name = "squad")
    private String squad;


    @Column(name = "pic")
    private byte[] pic;
}
