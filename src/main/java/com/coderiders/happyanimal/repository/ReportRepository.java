package com.coderiders.happyanimal.repository;

import com.coderiders.happyanimal.model.Report;
import com.coderiders.happyanimal.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportRepository extends JpaRepository<Report, Long> {
    @NotNull
    Page<Report> findAll(@NotNull Pageable pageable);

    Page<Report> findAllByUser(User User, Pageable pageable);
}
