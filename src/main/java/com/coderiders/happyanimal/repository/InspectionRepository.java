package com.coderiders.happyanimal.repository;

import com.coderiders.happyanimal.model.Animal;
import com.coderiders.happyanimal.model.Inspection;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface InspectionRepository extends JpaRepository<Inspection, Long> {
    @NotNull
    Page<Inspection> findAll(@NotNull Pageable pageable);

    Optional<Inspection> findByDate(LocalDate localDate);

    Optional<Inspection> findByAnimalListContaining(Animal animal);
}

